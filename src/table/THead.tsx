import React from 'react';
import { FaSort, FaSortUp, FaSortDown } from 'react-icons/fa';
import { HeaderProps } from '../helpers';

interface IconPropsType{
  propName?: HeaderProps;
  sortProp: HeaderProps | null;
  order: number;
}

export interface THeadConfigType{
  name: HeaderProps;
  display?: boolean;
  className?: string;
  sort?: boolean;
}

type THeadPropsType = IconPropsType & {
  headData: THeadConfigType[];
  setSortOpt: Function;
}

const Icon: React.FC<IconPropsType> = ({ propName, sortProp, order }: IconPropsType) => {
  if (propName !== sortProp) return <FaSort />;
  if (order === 1) return <FaSortDown />;
  return <FaSortUp />;
};

const THead: React.FC<THeadPropsType> = ({
  headData, setSortOpt, sortProp, order,
}: THeadPropsType) => (
  <thead className="text-secondary bg-light">
    <tr>
      {
        headData.map((col): JSX.Element | null => {
          if (col.sort !== false && col.display !== false) {
            return (
              <th onClick={(): void => setSortOpt(col.name)}>
                {col.name}
                <Icon propName={col.name} sortProp={sortProp} order={order} />
              </th>
            );
          }
          if (col.display !== false) return <th>{col.name}</th>;
          return null;
        })
      }
    </tr>
  </thead>
);

export default THead;
