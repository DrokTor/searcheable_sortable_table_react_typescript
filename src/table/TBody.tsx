import React from 'react';
import { DataType } from '../helpers';

interface Props {
  data: DataType[];
}

const TBody: React.FC<Props> = ({ data }: Props) => (
  <tbody className="text-secondary">
    {
    data.map((elem: DataType) => (
      <tr key={elem.id}>
        <td className="text-center">
          <img src={elem.picture} alt="logo" />
        </td>
        <td>{elem.name}</td>
        <td>{elem.gender}</td>
        <td>{elem.company}</td>
        <td>{elem.email}</td>
        <td className={`text-center ${elem.isActive ? 'text-success' : 'text-secondary'} `}>
          {elem.isActive ? <span>#yes</span> : <span>#no</span>}
        </td>
      </tr>
    ))
  }
  </tbody>
);

export default TBody;
