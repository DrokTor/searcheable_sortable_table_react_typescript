import React, { useState } from 'react';
import data from '../data/generated.json';
import {
  HeaderProps, SortOptType, Pipe, Filter, Sort,
} from '../helpers';
import THead, { THeadConfigType } from './THead';
import TBody from './TBody';

const Table: React.FC = () => {
  const [filter, setFilter] = useState<string>('');
  const [sort, setSort] = useState<SortOptType>({ prop: null, order: 1 }); // 1 for descending
  let timer: number;

  const search = (text: string): void => {
    clearTimeout(timer); // clear the timer bellow to avoid searching on every keystroke
    timer = window.setTimeout(() => setFilter(text), 700);
  };

  const setSortOpt = (prop: HeaderProps): void => {
    if (prop === sort.prop) setSort({ ...sort, order: sort.order * -1 });
    else setSort({ prop, order: 1 });
  };

  // create the pipe with filter & sort params locked in
  const getResult: Function = Pipe(Filter(filter), Sort(sort));

  // the config info to get passed to THead
  const headData: THeadConfigType[] = [
    { name: 'picture', className: 'text-center', sort: false },
    { name: 'id', display: false },
    { name: 'name' },
    { name: 'gender' },
    { name: 'company' },
    { name: 'email' },
    { name: 'isActive', className: 'text-center' },
  ];

  return (
    <div className="App col-10 mx-auto pt-5">
      <input
        type="text"
        placeholder="search all fields,  for status: #yes|#no "
        className="float-left mb-3 col-4"
        onChange={(e): void => search(e.target.value)}
      />
      <table className="table table-striped table-sm text-left ">
        <THead
          headData={headData}
          setSortOpt={setSortOpt}
          sortProp={sort.prop}
          order={sort.order}
        />
        <TBody data={getResult(data)} />
      </table>
    </div>
  );
};

export default Table;
