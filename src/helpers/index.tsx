// type of the main data in generated.json
export interface DataType{
  id: string;
  name: string;
  gender: string;
  company: string;
  email: string;
  isActive: boolean;
  picture: string;
}

// geting the keys from DataType
export type HeaderProps = keyof DataType;

// Sorting input type
export interface SortOptType{
  prop: HeaderProps | null;
  order: number;
}

// piping closure, to allow flow with multiple functions, Filter & Sort in our case
export const Pipe = (...fns: Function[]): Function => (data: DataType[]): DataType[] => (
  fns.reduce((v: DataType[], f: Function) => f(v), data)
);

// Filter function closure, locks the search input in the first call
// and execute it against the data on the second call when used inside the pipe
export const Filter = (filter: string): Function => (data: DataType[]): DataType[] => data.filter(
  (elem: DataType) => elem.name.match(new RegExp(filter, 'i'))
   || elem.gender.match(new RegExp(filter, 'i'))
   || elem.email.match(new RegExp(filter, 'i'))
   || elem.company.match(new RegExp(filter, 'i'))
   || ((): boolean => {
     if (filter === '#yes') return elem.isActive === true;
     if (filter === '#no') return elem.isActive === false;
     if (filter === '') return true;
     return false;
   })(),
);

// Sort function closure, locks the sort params in the first call
// and execute it against the data on the second call when used inside the pipe
// String(a[prop]) is used to cast the boolean isActive
export const Sort = ({ prop, order }: SortOptType): Function => ((elem: DataType[]): DataType[] => (
  prop === null ? elem : elem.sort((a: DataType, b: DataType): number => (
    order * (String(a[prop])).toLowerCase().localeCompare(String(b[prop]).toLowerCase())
  )))
);
